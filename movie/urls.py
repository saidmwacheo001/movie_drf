
from django.contrib import admin
from django.db import router
from django.urls import path,include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register('movies', viewset=views.MovieViewSet)



urlpatterns = [
    path('api/',include(router.urls)),
]
